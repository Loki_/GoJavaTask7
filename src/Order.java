import java.util.Currency;


/**
 * Created by anna on 10.12.16.
 */
public class Order {
    private long id;
    private int price;
    private Currency currency;
    private String itemName;
    private String shopIdentificator;
    private User user;

    public Order(long id, int price, Currency currency, String itemName, String shopIdentificator, User user){
        this.id = id;
        this.price = price;
        this.currency = currency;
        this.itemName = itemName;
        this.shopIdentificator = shopIdentificator;
        this.user = user;
    }

    public long getId(){
        return this.id;
    }
    public void setId(long id){
        this.id = id;
    }
    public int getPrice() {
        return this.price;
    }
    public void setPrice(int price){
        this.price = price;
    }
    public Currency getCurrency(){
        return this.currency;
    }
    public void setCurrency(Currency currency){
        this.currency = currency;
    }
    public String getItemName(){
        return this.itemName;
    }
    public void setItemName(String itemName){
        this.itemName = itemName;
    }
    public String getShopIdentificator(){
        return this.shopIdentificator;
    }
    public void setShopIdentificator(String shopIdentificator){
        this.shopIdentificator = shopIdentificator;
    }
    public User getUser(){
        return this.user;
    }
    public void setUser(User user){
        this.user = user;
    }
    @Override
    public boolean equals(Object object){
        final Order order = (Order) object;
        if (this.id == order.id && this.price == order.price && this.currency.equals(order.currency) && this.itemName.equals(order.itemName) && this.shopIdentificator.equals(order.shopIdentificator) && this.user.equals(order.user)){
            return true;
        } else
            return false;
    }
    @Override
    public int hashCode(){
        int result = 17;
        result = 31 * result + (int) id;
        result = 31 * result + price;
        result = 31 * result + currency.hashCode();
        result = 31 * result + itemName.hashCode();
        result = 31 * result + shopIdentificator.hashCode();
        result = 31 * result + user.hashCode();
        return result;
    }

}
